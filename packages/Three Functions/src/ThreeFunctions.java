import java.util.Scanner;

public class ThreeFunctions {
    public static int Surface(int l,int w){
            return(l*w);
    }
    public static void Max_Min(int [] Tab){
       int i;
       int min=Tab[0];
       int max=Tab[0];
       for( i=0;i<4;i++){
           if(Tab[i]>=max){ max=Tab[i];  }
           if(Tab[i]<=max){ min=Tab[i];  }
       }
       System.out.println(" Max : "+max+"\n Min : "+min);  
    }
    public static void Tri(int [] Tab){
        int i,j,e;
        for(i=0;i<7;i++){
            for(j=i+1;j<7;j++){
                if(Tab[j]>Tab[i]){
                    e=Tab[j];
                    Tab[j]=Tab[i];
                    Tab[i]=e;
                }
            }
        }
       System.out.println("Tri :");
       for( i=0;i<7;i++){
           System.out.println("Num "+(i+1)+": "+Tab[i]);
       }
    }
     
    
    public static void main(String[] args) {
       Scanner sc=new Scanner(System.in);
       //##########Max-- Min#########
       int[] Tab = new int[4];
       int i;
       for( i=0;i<4;i++){
           System.out.print("Number "+(i+1)+": ");
           Tab[i]=sc.nextInt();
       }
       Max_Min(Tab);
       //##########-Rectangle-#########
       System.out.print("Enter The Length Of Rectangle : L= ");
       int l=sc.nextInt();
       System.out.print("Enter The Width Of Rectangle : W= ");
       int w=sc.nextInt();
       System.out.println("Surface Of Rectangle : S= "+Surface(l,w));
       //##########-Tri 7 Numbers-#########
       int[] Tabt = new int[7];
        for( i=0;i<7;i++){
           System.out.print("Number "+(i+1)+": ");
           Tabt[i]=sc.nextInt();
       }
       Tri(Tabt);
       
    }
}
